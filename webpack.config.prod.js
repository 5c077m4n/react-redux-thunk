const webpack = require('webpack');
const path = require('path');
const glob = require('glob');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const webpackBundleAnalyzer = require('webpack-bundle-analyzer');
const { GenerateSW } = require('workbox-webpack-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

process.env.NODE_ENV = 'production';

module.exports = {
	// resolve: {
	// 	modules: [path.resolve('./node_modules'), path.resolve('./src/api')],
	// },
	mode: 'production',
	target: 'web',
	devtool: 'source-map',
	entry: {
		index: './src/index',
		HomePage: './src/components/home/HomePage',
		AboutPage: './src/components/about/AboutPage',
		CoursesPage: './src/components/courses/CoursesPage',
		ManageCoursePage: './src/components/courses/ManageCoursePage',
	},
	optimization: {
		splitChunks: { chunks: 'all' },
		minimizer: [new UglifyJsPlugin()],
	},
	output: {
		path: path.resolve(__dirname, 'build'),
		publicPath: '/',
		filename: '[name].[contenthash].bundle.js',
	},
	plugins: [
		new GenerateSW(),
		new webpackBundleAnalyzer.BundleAnalyzerPlugin({
			analyzerMode: 'static',
		}),
		new MiniCssExtractPlugin({
			filename: '[name].[contenthash].css',
		}),
		new PurgecssPlugin({
			paths: glob.sync(`${path.resolve(__dirname, 'src')}/**/*`, {
				nodir: true,
			}),
		}),
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': JSON.stringify('production'),
			'process.env.API_URL': JSON.stringify('http://localhost:3001'),
		}),
		new HtmlWebpackPlugin({
			template: 'src/index.html',
			// favicon: 'src/favicon.ico',
			minify: {
				removeComments: true,
				collapseWhitespace: true,
				removeRedundantAttributes: true,
				useShortDoctype: true,
				removeEmptyAttributes: true,
				removeStyleLinkTypeAttributes: true,
				keepClosingSlash: true,
				minifyJS: true,
				minifyCSS: true,
				minifyURLs: true,
			},
		}),
		new CompressionPlugin({
			algorithm: 'gzip',
			compressionOptions: { level: 9 },
			threshold: 8192,
			minRatio: 0.8,
		}),
	],
	module: {
		rules: [
			{
				test: /\.(m?jsx?)$/,
				exclude: /node_modules/,
				use: ['babel-loader', 'eslint-loader'],
			},
			{
				test: /(\.css)$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							sourceMap: true,
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							plugins: () => [require('cssnano')],
							sourceMap: true,
						},
					},
				],
			},
		],
	},
};
