const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

process.env.NODE_ENV = 'development';

module.exports = {
	// resolve: {
	// 	modules: [path.resolve('./node_modules'), path.resolve('./src/api')],
	// },
	mode: 'development',
	target: 'web',
	devtool: 'cheap-module-source-map',
	entry: {
		index: './src/index',
		HomePage: './src/components/home/HomePage',
		AboutPage: './src/components/about/AboutPage',
		CoursesPage: './src/components/courses/CoursesPage',
		ManageCoursePage: './src/components/courses/ManageCoursePage',
	},
	output: {
		path: path.resolve(__dirname, 'build'),
		publicPath: '/',
		filename: '[name].[contenthash].bundle.js',
		chunkFilename: '[name].[contenthash].bundle.js',
	},
	optimization: {
		splitChunks: { chunks: 'all' },
	},
	devServer: {
		stats: 'minimal',
		overlay: true,
		historyApiFallback: true,
		disableHostCheck: true,
		headers: { 'Access-Control-Allow-Origin': '*' },
		https: false,
		compress: false,
		port: 3000,
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env.API_URL': JSON.stringify('http://localhost:3001'),
		}),
		new HtmlWebpackPlugin({
			template: 'src/index.html',
			// favicon: "src/favicon.ico"
		}),
	],
	module: {
		rules: [
			{
				test: /\.m?jsx?$/,
				exclude: '/node_modules/',
				use: ['babel-loader', 'eslint-loader'],
			},
			{ test: /\.css$/, use: ['style-loader', 'css-loader'] },
		],
	},
};
