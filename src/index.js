import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';
import 'cross-fetch/polyfill';
import 'bootstrap/dist/css/bootstrap.min.css';

import configStore from './redux/configStore';
import App from './components/App';
import './index.css';

const store = configStore();

render(
	<ReduxProvider store={store}>
		<Router>
			<App />
		</Router>
	</ReduxProvider>,
	document.getElementById('root')
);
