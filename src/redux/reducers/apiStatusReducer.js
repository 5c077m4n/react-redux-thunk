import types from '../constants';
import initialState from './initialState';

const actionTypeEndsInSuccess = actionType => {
	return actionType.match(/(_SUCCESS|_ERROR)$/);
};

export default function apiCallStatusReducer(
	state = initialState.apiCallsInProgress,
	action
) {
	if (action.type === types.BEGIN_API_CALL) return state + 1;
	else if (actionTypeEndsInSuccess(action.type)) return state - 1;
	else return state;
}
