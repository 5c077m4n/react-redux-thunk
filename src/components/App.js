import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './common/Header';
import PageNotFound from './common/PageNotFound';

const HomePage = lazy(() => import('./home/HomePage'));
const AboutPage = lazy(() => import('./about/AboutPage'));
const CoursesPage = lazy(() => import('./courses/CoursesPage'));
const ManageCoursePage = lazy(() => import('./courses/ManageCoursePage'));

const App = () => (
	<div className="contianer-fluid">
		<Header />
		<Suspense fallback={<div>Loading...</div>}>
			<Switch>
				<Route exact path="/" component={HomePage} />
				<Route path="/about" component={AboutPage} />
				<Route path="/courses" component={CoursesPage} />
				<Route path="/course/:slug" component={ManageCoursePage} />
				<Route path="/course" component={ManageCoursePage} />
				<Route component={PageNotFound} />
			</Switch>
		</Suspense>
		<ToastContainer autoClose={3000} />
	</div>
);

export default App;
