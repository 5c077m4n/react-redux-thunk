import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';

import { loadCourses, saveCourse } from '../../redux/actions/courseActions';
import { loadAuthors } from '../../redux/actions/authorActions';
import { newCourse } from '../../../tools/mockData';
import CourseForm from './CourseForm';
import Spinner from '../common/spinner/Spinner';

export const ManageCoursesPage = ({
	courses,
	authors,
	saveCourse,
	loadCourses,
	loadAuthors,
	history,
	...props
}) => {
	const [course, setCourse] = useState({ ...props.course });
	const [errors, setErrors] = useState({});
	const [saving, setSaving] = useState(false);

	useEffect(() => {
		if (!courses.length) loadCourses();
		else setCourse({ ...props.course });
		if (!authors.legth) loadAuthors();
	}, [props.course]);

	const validateForm = () => {
		const { title, authorId, category } = course;
		const errors = {};

		if (!(title && title.length)) errors.title = 'The title is required.';
		if (!authorId) errors.title = 'The author name is required.';
		if (!(category && category.length))
			errors.title = 'The category is required.';

		setErrors(errors);
		return Object.keys(errors).length === 0;
	};
	const handleChange = e => {
		const { name, value } = e.target;
		setCourse(prevCourse => ({
			...prevCourse,
			[name]: name === 'authorId' ? +value : value,
		}));
	};
	const handleSave = e => {
		e.preventDefault();
		if (!validateForm()) return;

		setSaving(true);
		saveCourse(course)
			.then(() => toast.success('Course saved.'))
			.then(() => history.push('/courses'))
			.catch(error => {
				setSaving(false);
				setErrors({ onSave: error.message });
			});
	};

	return !authors.length || !courses.length ? (
		<Spinner />
	) : (
		<CourseForm
			course={course}
			errors={errors}
			authors={authors}
			onChange={handleChange}
			onSave={handleSave}
			saving={saving}
		/>
	);
};
ManageCoursesPage.propTypes = {
	history: PropTypes.object.isRequired,
	loadCourses: PropTypes.func.isRequired,
	saveCourse: PropTypes.func.isRequired,
	loadAuthors: PropTypes.func.isRequired,
	course: PropTypes.object.isRequired,
	courses: PropTypes.arrayOf(PropTypes.shape({ title: PropTypes.string })),
	authors: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.number,
			name: PropTypes.string,
		})
	),
};

const getCourseBySlug = (courses, slug) => {
	return courses.find(c => c.slug === slug);
};

const mapStateToProps = ({ courses, authors }, ownProps) => {
	const slug = ownProps.match.params.slug;
	const course =
		slug && courses.length ? getCourseBySlug(courses, slug) : newCourse;

	return {
		course,
		courses,
		authors,
	};
};
const mapDispatchToProps = { loadCourses, loadAuthors, saveCourse };

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ManageCoursesPage);
