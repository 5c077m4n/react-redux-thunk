import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { toast } from 'react-toastify';

import * as courseActions from '../../redux/actions/courseActions';
import * as authorActions from '../../redux/actions/authorActions';
import CourseList from './CourseList';
import Spinner from '../common/spinner/Spinner';

class CoursesPage extends Component {
	static propTypes = {
		actions: PropTypes.shape({
			loadCourses: PropTypes.func.isRequired,
			deleteCourse: PropTypes.func.isRequired,
			loadAuthors: PropTypes.func.isRequired,
		}).isRequired,
		courses: PropTypes.arrayOf(
			PropTypes.shape({ title: PropTypes.string })
		),
		authors: PropTypes.arrayOf(
			PropTypes.shape({
				id: PropTypes.number,
				name: PropTypes.string,
			})
		),
		loading: PropTypes.bool.isRequired,
	};
	state = { redirectToAddCoursePage: false };

	componentDidMount() {
		const { courses, authors, actions } = this.props;
		if (!courses.length) actions.loadCourses();
		if (!authors.legth) actions.loadAuthors();
	}

	onDelete = async course => {
		toast.success('Course deleted successfully!');
		try {
			await this.props.actions.deleteCourse(course);
		} catch (error) {
			toast.error(`The delete has failed: ${error.message}`, {
				autoClose: false,
			});
		}
	};

	render() {
		return (
			<>
				{this.state.redirectToAddCoursePage && (
					<Redirect to="/course" />
				)}
				<h2>Courses</h2>
				{this.props.loading ? (
					<Spinner />
				) : (
					<>
						<button
							style={{ marginBottom: 20 }}
							className="btn btn-primary add-course"
							onClick={() =>
								this.setState({ redirectToAddCoursePage: true })
							}>
							Add Course
						</button>
						<CourseList
							courses={this.props.courses}
							onDeleteClick={this.onDelete}
						/>
					</>
				)}
			</>
		);
	}
}

const mapStateToProps = ({ courses, authors, apiCallsInProgress }) => {
	if (!authors.length) return { courses: [], authors: [], loading: true };
	return {
		courses: courses.map(course => ({
			...course,
			authorName: authors.find(author => author.id === course.authorId)
				.name,
		})),
		authors,
		loading: apiCallsInProgress > 0,
	};
};
const mapDispatchToProps = dispatch => ({
	actions: {
		loadCourses: bindActionCreators(courseActions.loadCourses, dispatch),
		deleteCourse: bindActionCreators(courseActions.deleteCourse, dispatch),
		loadAuthors: bindActionCreators(authorActions.loadAuthors, dispatch),
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CoursesPage);
