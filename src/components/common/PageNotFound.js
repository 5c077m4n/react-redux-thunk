import React from 'react';

const PageNotFound = () => <h1>Sorry, the requested page was not found.</h1>;

export default PageNotFound;
