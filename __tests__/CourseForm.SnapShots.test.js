import React from 'react';
import renderer from 'react-test-renderer';

import CourseForm from '../src/components/courses/CourseForm';
import { courses, authors } from '../tools/mockData';

let props;
describe('<CourseForm />', () => {
	beforeEach(() => {
		props = {
			course: courses[0],
			authors,
			onSave: jest.fn(),
			onChange: jest.fn(),
			saving: true,
		};
	});

	test('Should set submit button label to "Saving..." when saving is true', () => {
		props.saving = true;
		const tree = renderer.create(<CourseForm {...props} />);

		expect(tree).toMatchSnapshot();
	});

	test('Should set submit button label to "Save" when saving is false', () => {
		props.saving = false;
		const tree = renderer.create(<CourseForm {...props} />);

		expect(tree).toMatchSnapshot();
	});
});
