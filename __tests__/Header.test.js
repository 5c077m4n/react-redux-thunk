import React from 'react';
import Header from '../src/components/common/Header';
import { mount, shallow } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';

describe('<Header />', () => {
	test('Should contain 3 NavLinks via shallow', () => {
		const wrapper = shallow(<Header />);
		expect(wrapper.find('NavLink').length).toBe(3);
	});

	test('Should contain 3 NavLinks via mount', () => {
		const wrapper = mount(
			<MemoryRouter>
				<Header />
			</MemoryRouter>
		);
		expect(wrapper.find('a').length).toBe(3);
	});
});
