import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import configMockStore from 'redux-mock-store';

import * as courseActions from '../src/redux/actions/courseActions';
import types from '../src/redux/constants';
import { courses } from '../tools/mockData';

const mockStore = configMockStore([thunk]);

describe('Async actions', () => {
	afterEach(() => fetchMock.restore());

	describe('Load Courses Thunk', () => {
		it('should create BEGIN_API_CALL and LOAD_COURSES_SUCCESS when loading courses', () => {
			const expectedActions = [
				{ type: types.BEGIN_API_CALL },
				{ type: types.LOAD_COURSES_SUCCESS, courses },
			];
			const store = mockStore({ courses: [] });

			fetchMock.mock('*', {
				body: courses,
				headers: { 'content-type': 'application/json' },
			});

			return store.dispatch(courseActions.loadCourses()).then(() => {
				expect(store.getActions()).toEqual(expectedActions);
			});
		});
	});
});

describe('createCourseSuccess', () => {
	test('Should create a CREATE_COURSE_SUCCESS action', () => {
		const course = courses[0];
		const expectedAction = { type: types.CREATE_COURSE_SUCCESS, course };
		const action = courseActions.createCourseSuccess(course);

		expect(action).toEqual(expectedAction);
	});
});
