import React from 'react';
import { shallow } from 'enzyme';

import CourseForm from '../src/components/courses/CourseForm';

const renderCourseForm = (args = {}) => {
	const defaultProps = {
		courses: [],
		course: {},
		authors: [],
		onSave: () => {},
		onChange: () => {},
		saving: false,
	};
	const props = { ...defaultProps, ...args };
	return shallow(<CourseForm {...props} />);
};

describe('<CourseForm />', () => {
	test('Renders header and from', () => {
		const wrapper = renderCourseForm();
		// console.log(wrapper.debug());
		expect(wrapper.find('h2').text()).toEqual('Add Course');
		expect(wrapper.find('form').length).toBe(1);
	});

	test('Should set submit button label to "Save" when saving is false', () => {
		const wrapper = renderCourseForm({ saving: false });
		expect(wrapper.find('button').text()).toBe('Save');
	});
	test('Should set submit button label to "Saving..." when saving is true', () => {
		const wrapper = renderCourseForm({ saving: true });
		expect(wrapper.find('button').text()).toBe('Saving...');
	});
});
