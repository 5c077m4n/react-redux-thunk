import React from 'react';
import { mount } from 'enzyme';
import { authors, courses, newCourse } from '../tools/mockData';
import { ManageCoursesPage } from '../src/components/courses/ManageCoursePage';

const render = (args = {}) => {
	let defaultProps = {
		authors,
		courses,
		course: newCourse,
		history: {},
		saving: false,
		errors: {},
		saveCourse: jest.fn(),
		loadAuthors: jest.fn(),
		loadCourses: jest.fn(),
		match: {},
	};

	const props = { ...defaultProps, ...args };
	return mount(<ManageCoursesPage {...props} />);
};

describe('<ManageCoursesPage />', () => {
	test('Should show errors if saved with an empty title', () => {
		const wrapper = render();
		wrapper.find('form').simulate('submit');
		const error = wrapper.find('.alert').first();
		expect(error.text()).toBe('The category is required.');
	});
});
