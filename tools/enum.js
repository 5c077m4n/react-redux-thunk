export default class Enum {
	constructor(...constList) {
		const self = this;
		constList.map(constant => {
			if (constant in this) throw new Error('Cannot repeat values.');
			self[constant] = constant;
		});
		Object.freeze(this);
	}
}
